﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;
using RandomCriterion.Model;

namespace RandomCriterion.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        #region Members
        
        private static volatile MainViewModel _instance;
        private static object _syncRoot = new Object();

        private string filename;
        private string userList;

        #endregion

        #region Constructors
        private MainViewModel() { }

        public static MainViewModel Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_syncRoot)
                    {
                        if (_instance == null)
                            _instance = new MainViewModel();
                    }
                    MainViewModel.Instance.Creator();
                }

                return _instance;
            }
        }
        #endregion

        #region Methods

        private void Creator()
        {
            RandomMT = new MT19937();
            AlgoEvaluation = new MethodEvaluation();
            TableEvaluation = new MethodEvaluation();
            UserEvaluation = new SequenceEvaluation();
            UserList = "";
        }

        private void OnDataChanged()
        {
            /*OnPropertyChanged("Filename");
            OnPropertyChanged("LocalPeer");
            OnPropertyChanged("PeerList");*/
        }
        
        #endregion

        #region Properties

        public MethodEvaluation AlgoEvaluation { get; set; }
        public MethodEvaluation TableEvaluation { get; set; }
        public SequenceEvaluation UserEvaluation { get; set; }
        public string UserList
        {
            get { return userList; }
            set { userList = value; }
        }
        

        public MT19937 RandomMT { get; set; }

        #endregion

        #region Commands

        

        #region Generate

        void GenerateExecute()
        {
            AlgoEvaluation.Units.GenerateSequence(0,9,1000);
            AlgoEvaluation.Tens.GenerateSequence(10,99,1000);
            AlgoEvaluation.Hundreds.GenerateSequence(100,999,1000);

            TableEvaluation.Units.GenerateSequence("one-digit.txt");
            TableEvaluation.Tens.GenerateSequence("two-digit.txt");
            TableEvaluation.Hundreds.GenerateSequence("three-digit.txt");
        }

        bool CanGenerateExecute()
        {
            return true;
        }
        public ICommand GenerateCommand
        {
            get { return new RelayCommand(param => this.GenerateExecute(), param => this.CanGenerateExecute()); }
        }
        
        #endregion

        #region Assess

        void AssessExecute()
        {
            AlgoEvaluation.Units.EvaluateSequence();
            AlgoEvaluation.Tens.EvaluateSequence();
            AlgoEvaluation.Hundreds.EvaluateSequence();

            TableEvaluation.Tens.EvaluateSequence();
            TableEvaluation.Units.EvaluateSequence();
            TableEvaluation.Hundreds.EvaluateSequence();

            try
            {
                var list = UserList.Split(',').ToList();
                UserEvaluation.Sequence = new ObservableCollection<int>();
                foreach (var num in list)
                {
                    UserEvaluation.Sequence.Add(Convert.ToInt32(num));
                }
                UserEvaluation.EvaluateSequence();
            }
            catch (Exception)
            {
                
            }
        }

        bool CanAssessExecute()
        {
            return true;
        }
        public ICommand AssessCommand
        {
            get { return new RelayCommand(param => this.AssessExecute(), param => this.CanAssessExecute()); }
        }

        #endregion

        #endregion
    }
}
