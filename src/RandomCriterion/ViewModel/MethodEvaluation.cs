﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RandomCriterion.ViewModel
{
    public class MethodEvaluation : ViewModelBase
    {
        public MethodEvaluation()
        {
            Units = new SequenceEvaluation();
            Tens = new SequenceEvaluation();
            Hundreds = new SequenceEvaluation();
        }

        public SequenceEvaluation Units { get; set; }
        public SequenceEvaluation Tens { get; set; }
        public SequenceEvaluation Hundreds { get; set; }
    }

    public class SequenceEvaluation : ViewModelBase
    {
        private ObservableCollection<int> sequence;
        private double probability;

        public SequenceEvaluation()
        {
            Sequence = new ObservableCollection<int>();
        }

        public ObservableCollection<int> Sequence
        {
            get { return sequence; }
            set { sequence = value; OnPropertyChanged("Sequence"); }
        }

        public double Probability
        {
            get { return probability; }
            set { probability = value; OnPropertyChanged("Probability"); }
        }

        public void GenerateSequence(int low, int high, int count)
        {
            Sequence = new ObservableCollection<int>();
            for (int i = 0; i < count; i++)
            {
                Sequence.Add(MainViewModel.Instance.RandomMT.RandomRange(low,high));
            }
        }

        public void GenerateSequence(string filename)
        {
            try
            {
                var text = File.ReadAllText(filename);
                var list = text.Split(' ').ToList();
                Sequence = new ObservableCollection<int>(list.Select(p => Convert.ToInt32(p)));
            }
            catch (Exception)
            {
            }
        }

        public void EvaluateSequence()
        {
            //double prob = (MeanProbability() + RangeProbability() + FrequencyProbability())/3;
            if (sequence.Count > 2)
            {
                double prob = (RangeProbability() + FrequencyProbability()) / 2;
                Probability = Math.Round(prob,2);
            }
           
        }

        private double MeanProbability()
        {
            // Арифметическое среднее
            var orderedSequence = sequence.OrderBy(p => p).ToList();
            double globalRange = orderedSequence.Last() - orderedSequence.First();
            double suposedMean = globalRange / 2;
            double mean = sequence.Average();
            double prop = mean > suposedMean ? suposedMean / mean : mean / suposedMean;
            return prop;
        }
        private double RangeProbability()
        {
            // Разность рядом стоящих элементов
            /* Считается разность между соседними элементами и делится на предыдущую разность. 
               Получаем отношение - чем оно ближе к нулю, тем больше разность между ренджами, тем случайнее числа */
            int prevRange = Math.Abs(sequence[1] - sequence[0]);
            double sum = 0;
            for (int i = 1; i < sequence.Count - 1; i++)
            {
                int newRange = Math.Abs(sequence[i + 1] - sequence[i]);
                if (newRange != prevRange)
                {
                    sum += newRange > prevRange ? (double)prevRange / newRange : (double)newRange / prevRange;
                }
                prevRange = newRange;
            }
            double prop = sum/sequence.Count();
            return prop;
        }

        private double FrequencyProbability()
        {
            // Частота вхождения
            /* 1. Выделяем различные числа в последовательности 
               2. Для каждого числа проходим по последовательности и смотрим на расстояния между вхождениями этих чисел
               3. Делим следущее расстояние на предыдущее - получаем отношение. Чем оно ближе к нулю, 
               тем больше различаются ренджи, тем случайнее вхождение чисел в последовательность.*/
            var groupedSequence = sequence.GroupBy(p => p);
            double groupProb = 0;
            foreach (var group in groupedSequence)
            {
                if (group.Count() > 2)
                {
                    int prevIndex = sequence.IndexOf(group.Key);
                    int index = sequence.Skip(prevIndex + 1).ToList().IndexOf(group.Key) + prevIndex + 1;
                    int prevRange = index - prevIndex;
                    double sum = 0;
                    for (int i = index + 1; i < sequence.Count; i++)
                    {
                        if (sequence[i] == group.Key)
                        {
                            int newRange = i - index;
                            sum += newRange > prevRange ? (double)prevRange / newRange : (double)newRange / prevRange;
                            prevRange = newRange;
                            index = i;
                        }
                    }
                    double localProb = 1 - sum / (group.Count()-2);
                    groupProb += localProb; // может поменять на среднее значение localProb для всех груп
                }
                
            }
            groupProb /= groupedSequence.Count();
            return groupProb;
        }
    }

}
