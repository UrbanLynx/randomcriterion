using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Threading;


namespace MersenneTwister
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.Button button1;
		//private MersenneTwister.RandomMT	m_RandomMT;
		private MersenneTwister.MT19937 m_MT19937;
		private int [] m_DiceValues = new int[6];
		private System.Windows.Forms.Button button2;
		private int [] m_DiceHits = new int[6];

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			m_MT19937 = new MersenneTwister.MT19937();
			m_DiceValues.Initialize();
			m_DiceHits.Initialize();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(176, 72);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(192, 23);
			this.button1.TabIndex = 0;
			this.button1.Text = "Single Dice Roll";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(176, 96);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(192, 23);
			this.button2.TabIndex = 1;
			this.button2.Text = "Roll 1000 Times";
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(376, 125);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "Form1";
			this.Text = "Mersenne Twister Dice Rolling";
			this.TopMost = true;
			this.Paint += new System.Windows.Forms.PaintEventHandler(this.Form1_Paint);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			int dv = 0;
			m_DiceValues.Initialize();
			for(int loop = 0; loop < 6; ++loop)
			{
				dv = m_MT19937.RandomRange(1,6);
				//dv = m_RandomMT.D6(1);
				m_DiceValues.SetValue(dv, loop);
				dv -=1; // keep array in bounds
				m_DiceHits[dv] = m_DiceHits[dv] +1;
				DrawDice();
			}	
			this.Invalidate();
		}

		private void button2_Click(object sender, System.EventArgs e)
		{
			int dv = 0;
			m_DiceValues.Initialize();
			for(int oloop = 0; oloop < 1000; ++oloop)
			{
				for(int loop = 0; loop < 6; ++loop)
				{
					dv = m_MT19937.RandomRange(1,6);
					//dv = m_RandomMT.D6(1);
					m_DiceValues.SetValue(dv, loop);
					dv -=1; // keep array in bounds
					m_DiceHits[dv] = m_DiceHits[dv] +1;
				}	
				DrawDice();
			}
			this.Invalidate();
		}

		private void Form1_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			DrawDice();
		}

		private void DrawDice()
		{
			Graphics g = this.CreateGraphics();
			Pen BlackPen = new Pen(Color.Black);
			SolidBrush WhiteBrush = new SolidBrush(Color.White);
			SolidBrush BlackBrush = new SolidBrush(Color.Black);
			Font drawFont = new Font("Arial", 8);
			String textstring;

			int offsetX = 0;
			int X		= 10;
			int offsetY = 0;
			int die     = 0;
			for(int loop = 0; loop < 6; ++loop)
			{
				offsetX = loop * 60;
				offsetY = loop * 10;
				g.FillRectangle(WhiteBrush, offsetX + X, 10, 50, 50);
				g.DrawRectangle(BlackPen,   offsetX + X, 10, 50, 50);
	
				die = loop + 1;
				textstring = die.ToString();
				textstring += " = ";
				textstring += m_DiceHits[loop].ToString();
				g.DrawString(textstring ,drawFont,BlackBrush,10, offsetY + 60);

				// Now draw the face of the die
				int check = m_DiceValues[loop];
				switch( check ) 
				{
					case 1:
						g.FillEllipse(BlackBrush, offsetX + 30,30,10,10);
						break;
					case 2:
						g.FillEllipse(BlackBrush, offsetX + 15,15,10,10);
						g.FillEllipse(BlackBrush, offsetX + 45,45,10,10);
						break;
					case 3:
						g.FillEllipse(BlackBrush, offsetX + 15,15,10,10);
						g.FillEllipse(BlackBrush, offsetX + 30,30,10,10);
						g.FillEllipse(BlackBrush, offsetX + 45,45,10,10);
						break;
					case 4:
						g.FillEllipse(BlackBrush, offsetX + 15,15,10,10);
						g.FillEllipse(BlackBrush, offsetX + 15,45,10,10);
						g.FillEllipse(BlackBrush, offsetX + 45,45,10,10);
						g.FillEllipse(BlackBrush, offsetX + 45,15,10,10);
						break;
					case 5:
						g.FillEllipse(BlackBrush, offsetX + 15,15,10,10);
						g.FillEllipse(BlackBrush, offsetX + 15,45,10,10);
						g.FillEllipse(BlackBrush, offsetX + 30,30,10,10);
						g.FillEllipse(BlackBrush, offsetX + 45,15,10,10);
						g.FillEllipse(BlackBrush, offsetX + 45,45,10,10);
						break;
					case 6:
						g.FillEllipse(BlackBrush, offsetX + 15,15,10,10);
						g.FillEllipse(BlackBrush, offsetX + 15,30,10,10);
						g.FillEllipse(BlackBrush, offsetX + 15,45,10,10);
						g.FillEllipse(BlackBrush, offsetX + 45,15,10,10);
						g.FillEllipse(BlackBrush, offsetX + 45,30,10,10);
						g.FillEllipse(BlackBrush, offsetX + 45,45,10,10);
						break;
					default:
						break;
				}
			}
		}
	}
}



